#!/bin/python

import urllib.request
import argparse
import os
from bs4 import BeautifulSoup
from datetime import datetime
from math import fmod

class stockScraper():
    def __init__(self):
        try:
            file = open('stocks.db', 'r')
            self.stocks = eval(str(file.read()))
            file.close()
        except:
            self.stocks = ["None"]

    def removeStock(self, symbol):
        "Delete first list in db that matches the given symbol. Example: removeStock('T')"
        index = self.returnStock(symbol)
        if index == -1:
            print('%s was not found in database! Are you sure you typed the symbol correctly?' % symbol)
        else:
            self.stocks.pop(index)
            self.updatedb()

    def returnStock(self, symbol):
        "This finds the index for a given stock. In the case that a stock is not found, this returns -1. This can be expanded to search for other objects in a stock's listing, although it would take a bit of work."
        for i in range(1, len(self.stocks)):
            if symbol == self.stocks[i][0]:
                return i
        return -1

    def printStock(self, symbol):
        index = self.returnStock(symbol) 
        if index != -1:
            print(self.stocks[index][1])
            print()
            print("    Symbol: %s" % self.stocks[index][0])
            print("    Last:   %s" % round(self.stocks[index][4], 2))
            print("    Change: %s" % round(self.stocks[index][5], 2))
            print()
            if self.stocks[index][2] != None and self.stocks[index][3] != None:
                original_value = float(self.stocks[index][2]*self.stocks[index][3])
                total_value = float(self.stocks[index][2]*self.stocks[index][4])
                profit = float(total_value - original_value)
                print("    Shares: %s" % self.stocks[index][2])
                print("    Buy-in: %s" % self.stocks[index][3])
                print("    Value:  %s" % round(total_value, 2))
                print()
                print("    Profit: %s" % round(profit, 2))
                print()
                try:
                    dividend_print = "|"
                    for i in self.stocks[index][6]:
                        if i == 1:
                            dividend_print = dividend_print + "■|"
                        else:
                            dividend_print = dividend_print + " |"
                    print("|J|F|M|A|M|J|J|A|S|O|N|D|")
                    print(dividend_print)
                except Exception:
                    pass
            print()
        else:
            temp_stock = self.scrape(symbol)
            if temp_stock != -1:
                print(temp_stock[0])
                print()
                print("    Symbol:  %s" % symbol)
                print("    Last:    %s" % temp_stock[1])
                print("    Change: %s" % temp_stock[2])
                if temp_stock[3]:
                    print()
                    print(temp_stock[3])
                    print()

    def updatedb(self):
        self.stocks
        file = open('stocks.db', 'w')
        file.write(str(self.stocks))
        file.close()

    def recalldb(self):
        file = open('stocks.db', 'r')
        self.stocks = eval(str(file.read()))
        file.close()

    def addStock(self, symbol, shares, value):
        index = self.returnStock(symbol)
        if index == -1:
            temp = self.scrape(symbol)
            if type(temp) == None:
                return -1
            if shares == None or value == None:
                self.stocks.append([symbol, temp[0], None, None, temp[1], temp[2], temp[3]])
            else:
                self.stocks.append([symbol, temp[0], shares, value, temp[1], temp[2], temp[3]])

        else:
            if shares == None or value == None:
                return -1
            else:
                self.stocks[index][2] = shares
                self.stocks[index][3] = value

        self.updatedb()

    def updateStock(self, symbol, shares, value):
        index = self.returnStock(symbol)
        if index == -1:
            return -1

        temp = self.scrape(symbol)
        if type(temp) == None:
            if shares == None or value == None:
                return -1
            else:
                self.stocks[index][2] = shares
                self.stocks[index][3] = value
        else:
            if shares == None or value == None:
                self.stocks[index] = [symbol, temp[0], self.stocks[index][2], self.stocks[index][3], temp[1], temp[2], temp[3]]
            else:
                self.stocks[index] = [symbol, temp[0], shares, value, temp[1], temp[2], temp[3]]
        self.updatedb()

    def scrape(self, symbol):
        website = urllib.request.urlopen("https://bigcharts.marketwatch.com/quickchart/quickchart.asp?symb=%s" % symbol)

        try:
            soup = BeautifulSoup(website.read(), 'html.parser')
            full_name = soup.tr.select_one(".header").select_one(".fleft").findNext("div").contents[0]
            price = float(soup.td.select_one(".last").div.contents[0])
            change = float(soup.td.select_one(".change").div.span.contents[0])
        except:
            print(symbol, "failed to load! Are you sure it's valid?")
            return -1

        dividend_calendar = [0,0,0,0,0,0,0,0,0,0,0,0]

        try:
            website = urllib.request.urlopen('https://www.dividentinformation.com/search_ticker/?identifier=%s' % symbol)
            soup = BeautifulSoup(website.read(), 'html.parser')

            # If dividend.com doesn't have the most recent dividend info, try using the last payout.
            # If _that_ fails, finish out the function without dividend info.
            try:
                for i in range(1, 13)
                    dividend_this = soup.find('div', {'id': 'history'}).findAll('tbody')[1].findAll('tr')[i].find('td', {'width': '25%'}).contents[0]:
                    if(datetime.fromisoformat(dividend_last).year == datetime.now.year() - 1):
                        dividend_last = soup.find('div', {'id': 'history'}).findAll('tbody')[1].findAll('tr')[i].find('td', {'width': '25%'}).contents[0]:
                    elif(datetime.fromisoformat(dividend_last).year < datetime.now.year() - 1):
                        break

                dividend_last = soup.select_one('.dividend-history').findAll("td")[2].p.contents[0][1:11]
                dividend_last_month = datetime.fromisoformat(dividend_last).month
                dividend_frequency = soup.select_one('.dividend-history').findAll("td")[7].p.contents[0][1]
            except:
                dividend_last = soup.select_one('.dividend-history').findAll("td")[10].p.contents[0][1:11]
                dividend_last_month = datetime.fromisoformat(dividend_last).month
                dividend_frequency = soup.select_one('.dividend-history').findAll("td")[15].p.contents[0][1]

            if dividend_frequency == "M": # Month
                dividend_frequency = 1
            if dividend_frequency == "Q": # Quarter
                dividend_frequency = 3
            if dividend_frequency == "S": # Semi-annual
                dividend_frequency = 5
            if dividend_frequency == "A": # Annual
                dividend_frequency = 11

            for i in range(12):
                if fmod(dividend_last_month - i - 1, dividend_frequency) == 0:
                    dividend_calendar[i] = 1
        except Exception:
            pass
        return [full_name, price, change, dividend_calendar]

dbctl = stockScraper()

parser = argparse.ArgumentParser(description="Controls the database and stock scraper. Valid commands are 'add', 'print', 'remove', and 'update'. Each command is followed by a stock symbol. 'Add' and 'update' commands can also be followed by two integers, first for number of shares owned, and second for initial buy-in price.")
parser.add_argument('argument', type=str, nargs='?', help="Choose between 'add', 'print', 'remove', and 'update'.")
parser.add_argument('symbol', type=str, nargs='?', help="Stock symbol (not name) as a string. eg: 'T' for AT&T, or 'SBUX' for Starbucks")
parser.add_argument('shares', type=float, nargs='?', help="Number of shares owned as an integer.")
parser.add_argument('value', type=float, nargs='?', help="Buy-in price as a float. Must be nothing but numbers and a period.")
args = parser.parse_args()
if args.symbol is not None:
    args.symbol = args.symbol.upper()

args.argument = "update"
args.symbol = "T"

if args.argument == "add":
    if args.symbol is None:
        print("Please enter a symbol to add!")
        if args.shares is None or args.value is None:
            dbctl.addStock(args.symbol, None, None)
        else:
            dbctl.addStock(args.symbol, args.shares, args.value)

if args.argument == "print":
    if args.symbol is None:
        for i in dbctl.stocks[1::]:
            print("[%s] %s - %s shares @ $%s (%s)" % (i[0], i[1], i[2], i[4], i[5])) 
    else:
        dbctl.printStock(args.symbol)

if args.argument == "remove":
    if args.symbol is None:
        print("Please enter a symbol to remove!")
    else:
        dbctl.removeStock(args.symbol)

if args.argument == "update":
    if args.symbol == None:
        for i in dbctl.stocks[1::]:
            if type(i[0]) is str:
                dbctl.updateStock(i[0], None, None)
    else:
        if args.shares == None or args.value == None:
            dbctl.updateStock(args.symbol, None, None)
        else:
            dbctl.updateStock(args.symbol, args.shares, args.value)